package ex01;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;



public class ViewResult implements View{
private static final String FNAME = "items.bin";

private static final int DEFAULT_NUM = 3;

private ArrayList<Item2d> items = new ArrayList<Item2d>();

public ViewResult() {
	this (DEFAULT_NUM);
}

public ViewResult(int n) {
	for(int ctr = 0; ctr < n; ctr++) {
		items.add(new Item2d());
	}
}

public ArrayList<Item2d> getItems() {
	return items;
}
private int calcPer(int length, int width) {
	return (2 * length) + (2 * width) ;
}

/**
 * ��������� �������� �������. 
 * @param length - �������� ������.
 * @param width - �������� ������.
 * @return ��������� ���������� �������.
 */
private int calcSquare(int length, int width) {
	return length * width;
}

/**
 * ��������� �������� ������. 
 * @param length - �������� ������.
 * @param width - �������� ������.
 * @param height - �������� ������.
 * @return ��������� ���������� �������.
 */
private int calcVol(int length, int width, int height) {
	return length * width * height ;
}


public void ViewSave() throws IOException {
	ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
	os.writeObject(items);

	os.flush();
	os.close();
}

/**
 * ��������������� {@linkplain Calc#perimeter},{@linkplain Calc#square},{@linkplain Calc#volume} �� ����� {@linkplain Calc#FNAME}
 * * @throws Exception
 */


@SuppressWarnings("unchecked")
public void ViewRestore() throws Exception {
	ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
	items = (ArrayList<Item2d>) is.readObject();
	
	is.close();
}

public void viewInit(int parseInt, int parseInt2, int parseInt3) {
	
	Integer.parseInt("11101000", 2);
	Integer.parseInt("11001000", 2);
	Integer.parseInt("11101110", 2);
	// TODO Auto-generated method stub
	
}

public void viewHeader() {
	System.out.println("Result: ");
	
}

public void viewBody() {
	for(Item2d item : items) {
		System.out.println(item.calcPer(Integer.parseInt("101", 2),Integer.parseInt("111", 2)));
		System.out.println(item.calcSquare(Integer.parseInt("101", 2),Integer.parseInt("111", 2)));
		System.out.println(item.calcVol(Integer.parseInt("101", 2),Integer.parseInt("111", 2) ,Integer.parseInt("110", 2)));
		
	}
	System.out.println();
}

public void viewFooter() {
	System.out.println("END.");
}
public void viewShow() {
	viewHeader();
	viewBody();
	viewFooter();
	
}

@Override
public void viewSave() throws IOException {
	// TODO Auto-generated method stub
	
}

@Override
public void viewRestore() throws IOException {
	// TODO Auto-generated method stub
	
}

}
