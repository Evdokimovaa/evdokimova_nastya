package ex01;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * �������� ���������� ������� ��� ���������� � ����������� �����������.
 * @author nastya 
 * @version 1.0
 */
public class Calc {
	/** ��� �����, ������������ ��� ������������. */
	private static final String FNAME = "Item2d.bin";
	/** ��������� ��������� ���������� ���������. ������ ������ {@linkplain Item2d} */
	private Item2d perimeter;
	/** ��������� ��������� ���������� �������. ������ ������ {@linkplain Item2d} */
	private Item2d square;
	/** ��������� ��������� ���������� ������. ������ ������ {@linkplain Item2d} */
	private Item2d volume;

	/** �������������� {@linkplain Calc#perimeter}
	 *  �������������� {@linkplain Calc#square} 
	 *  �������������� {@linkplain Calc#volume} 
	  */
	public Calc() {
		perimeter = new Item2d();
		square = new Item2d();
		volume = new Item2d();
	}

/** ���������� �������� {@linkplain Calc#perimeter}  
  * @param perimeter - ����� �������� ������ �� ������ {@linkplain Item2d}  
  */  public void setPerimeter(Item2d perimeter) { 

 this.perimeter = perimeter;  }

  
  /** ���������� �������� {@linkplain Calc#square}  
   * @param square - ����� �������� ������ �� ������ {@linkplain Item2d}  
   */  public void setSquare(Item2d square) { 

  this.perimeter = perimeter;  }

   
   /** ���������� �������� {@linkplain Calc#volume}  
    * @param perimeter - ����� �������� ������ �� ������ {@linkplain Item2d}  
    */  public void setVolume(Item2d volume) { 

   this.volume = volume;  }

	/**
	 * �������� �������� {@linkplain Calc#perimeter}
	 * @return ������� �������� ������
	 * �� ������ {@linkplain Item2d}
	 */
	public Item2d getPerimeter() {
		return perimeter;
	}

	
	/**
	 * �������� �������� {@linkplain Calc#square}
	 * @return ������� �������� ������
	 * �� ������ {@linkplain Item2d}
	 */
	public Item2d getSquare() {
		return square;
	}

	
	/**
	 * �������� �������� {@linkplain Calc#volume}
	 * @return ������� �������� ������
	 * �� ������ {@linkplain Item2d}
	 */
	public Item2d getVolume() {
		return volume;
	}

	/**
	 * ��������� �������� ���������. 
	 * @param length - �������� ������.
	 * @param width - �������� ������.
	 * @return ��������� ���������� �������.
	 */
	private int calcPer(int length, int width) {
		return (2 * length) + (2 * width) ;
	}
	
	/**
	 * ��������� �������� �������. 
	 * @param length - �������� ������.
	 * @param width - �������� ������.
	 * @return ��������� ���������� �������.
	 */
	private int calcSquare(int length, int width) {
		return length * width;
	}
	
	/**
	 * ��������� �������� ������. 
	 * @param length - �������� ������.
	 * @param width - �������� ������.
	 * @param height - �������� ������.
	 * @return ��������� ���������� �������.
	 */
	private int calcVol(int length, int width, int height) {
		return length * width * height ;
	}



	/** ������� ��������� ����������.
	 * @param length - �������� ������.
	 * @param width - �������� ������.
	 * @param height - �������� ������. 
	 * */
	public void show(int length, int width, int growth) {
		int p = calcPer(length, width);
		System.out.println("Perimeter = " + p + "\n");
		int s = calcSquare( length, width);
		System.out.println("Square = " + s + "\n");
		int v = calcVol(length, width, growth);
		System.out.println("Volume = " + v + "\n");
	}

	/**
	 * ��������� {@linkplain Calc#perimeter},{@linkplain Calc#square},{@linkplain Calc#volume} � ����� {@linkplain Calc#FNAME} 
	 * @throws IOException
	 */
	public void save() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
		os.writeObject(perimeter);
		os.writeObject(square);
		os.writeObject(volume);
		os.flush();
		os.close();
	}

	/**
	 * ��������������� {@linkplain Calc#perimeter},{@linkplain Calc#square},{@linkplain Calc#volume} �� ����� {@linkplain Calc#FNAME}
	 * * @throws Exception
	 */
	public void restore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		perimeter = (Item2d) is.readObject();
		square = (Item2d) is.readObject();
		volume = (Item2d) is.readObject();
		is.close();
	}

	public void init(int parseInt, int parseInt2, int parseInt3) {
		
		Integer.parseInt("11101000", 2);
		Integer.parseInt("11001000", 2);
		Integer.parseInt("11101110", 2);
		// TODO Auto-generated method stub
		
	}

}