package ex01;
import java.io.IOException;
/**Template design 
 * Factory Method
 * Interface displayed object
 * @author nasty
 */
public interface View {
	/**View headline*/
	public void viewHeader();
	
	/**View body code*/
	public void viewBody();
	
	/**View footer*/
	public void viewFooter();
	
	/**View wholly object*/
	public void viewShow();
	
	/**Do init*/
	public void viewInit(int parseInt, int parseInt2, int parseInt3);
	
	/**Save data for next restore*/
	public void viewSave() throws IOException;
	
	/**Restore earlier save data*/
	public void viewRestore() throws IOException;

}
