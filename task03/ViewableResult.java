package ex01;
/**ConcreateCreator
 * Template design 
 * Factory Method
 * Interface displayed object
 * @author nasty
 * @see Viewable
 * @see Viewable#getView()
 */
public class ViewableResult implements Viewable{
	/**Create object get View
	 * {@linkplain ViewResult} 
	 */
	@Override
	public View getView() {
		return new ViewResult();
		
	}
	
}
