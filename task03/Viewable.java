package ex01;
/**Creator 
 * Template design 
 * Factory Method
 * Interface displayed object
 * @author nasty
 * @see Viewable#getView()
 */
public interface Viewable {
	/**Create object, which realize
	 * {@linkplain View}
	 */
	public View getView();

}
