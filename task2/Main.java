package task02;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * ���������� � ����������� �����������. �������� ���������� ������������ ������
 * main().
 * 
 * @author nastya
 * @version 1.0
 * @see Main#main
 */
public class Main {

	/**
	 * ������ ������ {@linkplain Calc}. <br>
	 * ������ ������ ���. �������.
	 */
	private Calc calc = new Calc();

	/** ���������� ����. */
	private void menu() {
		String s = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		do {
			do {
				System.out.println("Enter command...");
				System.out.print("'q'uit, 'v'iew, 'g'enerate,  's'ave, 'r'estore: ");
				try {
					s = in.readLine();
				} catch (IOException e) {
					System.out.println("Error: " + e);
					System.exit(0);
				}
			} while (s.length() != 1);
			switch (s.charAt(0)) {
			case 'q':
				System.out.println("Exit.");
				break;
			case 'v':
				System.out.println("View current.");
				calc.show(Integer.parseInt("101", 2),Integer.parseInt("111", 2),Integer.parseInt("1000", 2));
				break;
			case 'g':
				System.out.println("Random generation.");
				calc.init(Integer.parseInt("11101000", 2),Integer.parseInt("11001000", 2),Integer.parseInt("11101110", 2));
				calc.show(5,7,8);
				break;
			case 's':
				System.out.println("Save current.");
				try {
					calc.save();
				} catch (IOException e) {
					System.out.println("Serialization error: " + e);
				}
				calc.show(5, 7, 8);
				break;
			case 'r':
				System.out.println("Restore last saved.");
				try {
					calc.restore();
				} catch (Exception e) {
					System.out.println("Serialization error: " + e);
				}
				calc.show(5, 7, 8);
				break;
			default:
				System.out.print("Wrong command. ");
			}
		} while (s.charAt(0) != 'q');
	}

	/**
	 * ����������� ��� ������� ���������.
	 * ����������� �������� ������� ���
	 * ��������� ����������. 
	 * ���������� ���������� ��������� �� �����. 
	 * @param args - ��������� ������� ���������.
	 */
	public static void main(String[] args) {
		Main main = new Main();
		main.menu();
	}

}