package task02;

import java.io.Serializable;


/**
 * ������ �������� ������ � ��������� ����������.
 * @author nastya
 * @version 1.0
 */
public class Item2d implements Serializable {

	/** �������� ����� */
	// transient
	private int length;
	/** �������� ������ */
	private int width;

	/** �������� ������ */
	private int height;
	
	
	/** ������������� ��������������� ��������� */
	private static final long serialVersionUID = 1L;

	/** �������������� ���� {@linkplain Item2d#length}, {@linkplain Item2d#width}, {@linkplain Item2d#height} */
	public Item2d() {
		length = 0;
		
		width = 0;
		height = 0;
	}

	/**
	 * ������������� �������� �����: �����, ������ � ������.
	 * 
	 * @param length - �������� ��� ������������� ���� {@linkplain Item2d#length}
	 * @param width - �������� ��� ������������� ���� {@linkplain Item2d#width}
	 * @param height - �������� ��� ������������� ���� {@linkplain Item2d#height}
	 */
	public Item2d(int heigth, int width, int height) {
		this.height = height;
		this.width = width;
		this.height = height;

	}

	/**
	 * ��������� �������� ���� {@linkplain Item2d#length}
	 * 
	 * @param length - �������� ��� {@linkplain Item2d#length}
	 * @return �������� {@linkplain Item2d#length}
	 */
	public int setLength(int length) {
		return this.length = length;
	}
	
	
	/**
	 * ��������� �������� ���� {@linkplain Item2d#width}
	 * 
	 * @param x - �������� ��� {@linkplain Item2d#width}
	 * @return �������� {@linkplain Item2d#width}
	 */
public int setWidth(int width) {
	return this.width = width;
}
	
	
	/**
	 * ��������� �������� ���� {@linkplain Item2d#height}
	 * 
	 * @param x - �������� ��� {@linkplain Item2d#height}
	 * @return �������� {@linkplain Item2d#height}
	 */
	public int setHeight(int height) {
		return this.height = height;
	}

	/**
	 * ��������� �������� ���� {@linkplain Item2d#length}
	 * 
	 * @return �������� {@linkplain Item2d#length}
	 */
	public int getLength() {
		return length;
	}
	
	/**
	 * ��������� �������� ���� {@linkplain Item2d#width}
	 * 
	 * @return �������� {@linkplain Item2d#width}
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * ��������� �������� ���� {@linkplain Item2d#height}
	 * 
	 * @return �������� {@linkplain Item2d#height}
	 */
	public int getHeight() {
		return height;
	}



	/**
	 * ��������� �������� {@linkplain Item2d#length} � {@linkplain Item2d#width}� {@linkplain Item2d#height}
	 * 
	 * @param length - �������� ��� {@linkplain Item2d#length}
	 * @param width - �������� ��� {@linkplain Item2d#width}
	 * @param height - �������� ��� {@linkplain Item2d#height}
	 * @return this
	 */
	public Item2d setXY(int length, int width, int height) {
		this.length = length;
		this.width = width;
		this.height = height;
		return this;
	}

	
	
	/**
	 * ������������ ��������� ���������� � ���� ������.<br>
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "Length = " + length + "\n" + "Width = " + width + "\n" + "Height = " + height + "\n";
	}

}
